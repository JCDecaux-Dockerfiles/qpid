FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

RUN yum install supervisor qpid-cpp-server python-qpid-qmf python-qpid -y \
  && yum clean all

COPY supervisord.conf /etc/supervisord.conf

VOLUME ["/var/lib/qpidd"]

EXPOSE 5672

CMD ["/usr/bin/supervisord"]
